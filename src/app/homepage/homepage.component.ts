import {Component, OnChanges, OnInit} from '@angular/core';
import {UserService} from "../user.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  countries: any=[];
  constructor(private userService:UserService) {
    this.userService.getData().subscribe((data:any)=>{
      this.countries=data;
      console.log(this.countries);
    });

  }
  ngOnInit(): void {
  }

}
