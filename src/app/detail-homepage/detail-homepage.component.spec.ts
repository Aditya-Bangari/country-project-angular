import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailHomepageComponent } from './detail-homepage.component';

describe('DetailHomepageComponent', () => {
  let component: DetailHomepageComponent;
  let fixture: ComponentFixture<DetailHomepageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailHomepageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
