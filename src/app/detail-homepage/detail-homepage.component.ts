import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {UserService} from "../user.service";

@Component({
  selector: 'app-detail-homepage',
  templateUrl: './detail-homepage.component.html',
  styleUrls: ['./detail-homepage.component.css']
})
export class DetailHomepageComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private userService:UserService) { }
  alpha3Code: any;
  countryDetail: any = [];
  ngOnInit(): void {
    this.getCountryId()
  }
  getCountryId(): void {
    if (this.activatedRoute.snapshot.params.alpha3Code) {
      this.alpha3Code = this.activatedRoute.snapshot.params.alpha3Code;

      console.log(this.activatedRoute)

    }
    this.userService.getDetail(this.alpha3Code).subscribe((data:any)=>{
      this.countryDetail = data;
      console.log(this.countryDetail);
    })


    return
  }
}
