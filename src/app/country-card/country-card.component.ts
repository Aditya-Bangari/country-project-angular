import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {UserService} from "../user.service";

@Component({
  selector: 'app-country-card',
  templateUrl: './country-card.component.html',
  styleUrls: ['./country-card.component.css']
})
export class CountryCardComponent implements OnInit, OnChanges {
  @Input()  countries: any=[];
  c: any ;
  searchWord: string= "";
  search(event: any){
    if(event === ""){
      this.c = this.countries
    }
    else {
    this.c= this.countries.filter((event1: any)=>{
      return event1.name.toLowerCase().startsWith(event.toLowerCase());
      }
    );
    }
  }

  constructor() {}


  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.c = this.countries
  }

}
