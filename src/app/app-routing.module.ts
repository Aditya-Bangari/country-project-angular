import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomepageComponent} from "./homepage/homepage.component";
import {DetailHomepageComponent} from "./detail-homepage/detail-homepage.component";

const routes: Routes = [
  { path: 'details/:alpha3Code', component: DetailHomepageComponent },
  { path: '', component: HomepageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
