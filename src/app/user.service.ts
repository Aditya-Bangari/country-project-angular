import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient) { }
  getData()
  {
    let url = 'https://restcountries.eu/rest/v2/all';
    return this.http.get(url);
  }

  getDetail(alphacode: any)
  {
    let url1 = 'https://restcountries.eu/rest/v2/alpha/'+ alphacode
    return this.http.get(url1);
  }
}
